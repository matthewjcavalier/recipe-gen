;;;; recipe-gen.lisp

(in-package #:recipe-gen)


(defun prompt-read (prompt)
  (format *query-io* "~a: " prompt)
  (force-output *query-io*)
  (read-line *query-io*))


(defun get-info (prompt exit-cmd)
  (let ((ing-list NIL)
	(working T))
    (progn
      (format T "============================================~%")
      (loop while working
	 do (let ((curr-line (prompt-read prompt)))
	      (if (string= curr-line exit-cmd)
		  (setq working NIL)
		  (setq ing-list (concatenate 'list ing-list (list curr-line)))))))
    ing-list))

(defun gen-recipe ()
  (list :name (prompt-read "Recipe Name")
	:desc (prompt-read "Recipe Description")
	:ings (get-info "Enter Ing (Q to quit)" "Q")
	:steps (get-info "Enter Steps (Q to quit)" "Q")))

(defun gen-recipes ()
  (let ((recipe-list NIL))
    (loop while (y-or-n-p "Add another recipe [y/n]: ")
       do (setq recipe-list (concatenate 'list recipe-list (list (gen-recipe)))))
    recipe-list))

(defun jsonify-recipe (recipe)
  (let ((the-string
	 (cl-json:encode-json-plist-to-string recipe)))
    the-string))

(defun jsonify-recipes (recipes)
  (let ((recipes-json "[")
	(first-recipe T))
    (for:for ((recipe over recipes))
      (setq recipes-json (concatenate 'string recipes-json (if first-recipe (progn (setq first-recipe NIL) "") ",") (jsonify-recipe recipe))))
    (concatenate 'string recipes-json "]")))

(defun write-to-file (text &optional (output-file "./recipes.json"))
  (break)
  (let ((io-stream (open output-file :direction :output :if-exists :supersede)))
    (write-string text io-stream)
    (close io-stream)))
