;;;; recipe-gen.asd

(asdf:defsystem #:recipe-gen
  :description "Describe recipe-gen here"
  :author "Matt Cavalier <matt@cavlaier.dev>"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on (#:for
		  #:cl-json)
  :components ((:file "package")
               (:file "recipe-gen")))
